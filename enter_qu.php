<?php
session_start();
require_once 'includes/config.php';

$vendor_id	= filter_input(INPUT_GET, 'vendor_id');

$db = getDbInstance();
$db->where('id', $vendor_id);
$vendorData = $db->getOne('vendors');


?>
<?php include BASE_PATH.'/includes/header.php'; ?>
<div id="page-wrapper" class="max600">
    <div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">Kom erbij maatje bij: <?php echo $vendorData['name']; ?></h2>
        </div>
    </div>
    <!-- Flash messages -->
    <?php include BASE_PATH.'/includes/flash_messages.php'; ?>
    <form class="form" action="functions/enter_qu.php" method="post" id="customer_form" enctype="multipart/form-data">
        <?php include BASE_PATH.'/views/enter_qu_form.php'; ?>
    </form>
</div>

<?php include BASE_PATH.'/includes/footer.php'; ?>
