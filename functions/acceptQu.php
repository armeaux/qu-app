<?php
session_start();
require_once '../includes/config.php';
require_once BASE_PATH . '/includes/auth_validate.php';


$uuid = filter_input(INPUT_GET, 'uuid');

$update['status'] = 'completed';

$db = getDbInstance();
$db->where('uuid', $uuid);

if($db->update('queue', $update)) {
    $_SESSION['success'] = "Customer accepted successfully";
    header('Location: ../index.php');
    die;
} else {
    header('Location: ../index.php');
    die;
}





?>