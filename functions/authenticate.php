<?php
require_once '../includes/config.php';
session_start();


if ($_SERVER['REQUEST_METHOD'] === 'POST')
{
	$username = filter_input(INPUT_POST, 'username');
	$password = filter_input(INPUT_POST, 'password');
	$remember = filter_input(INPUT_POST, 'remember');

	// Get DB instance.
	$db = getDbInstance();

	$db->where('user_name', $username);
	$row = $db->getOne('admin_accounts');

	if ($db->count >= 1)
    {
		$db_password = $row['password'];
		$user_id = $row['id'];

		if (password_verify($password, $db_password))
        {
			$_SESSION['user_logged_in'] = TRUE;
			$_SESSION['admin_type'] = $row['admin_type'];
            $_SESSION['user_id'] = $row['id'];
			$_SESSION['vendor_id'] = $row['vendor_id'];

		
			header('Location: ../index.php');
		}
        else
        {
			$_SESSION['login_failure'] = 'Invalid user name or password';
			header('Location: ../login.php');
		}
		exit;
	}
    else
    {
		$_SESSION['login_failure'] = 'Invalid user name or password';
		header('Location: ../login.php');
		exit;
	}
}
else
{
	die('Method Not allowed');
}
