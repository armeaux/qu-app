<?php
session_start();
require_once '../includes/config.php';

// Serve POST method, After successful insert, redirect to customers.php page.
if ($_SERVER['REQUEST_METHOD'] === 'POST') 
{
    // Mass Insert Data. Keep "name" attribute in html form same as column name in mysql table.
    $requestData = array_filter($_POST);


    $curl = curl_init();

    curl_setopt_array($curl, array(
    CURLOPT_URL => 'http://phpstack-577290-1867358.cloudwaysapps.com/enterQu',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'POST',
    CURLOPT_POSTFIELDS => json_encode($requestData),
    CURLOPT_HTTPHEADER => array(
        'Authorization: kshd23ohoiduh97ouh2oeiu23h',
        'Content-Type: application/json'
    ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);

    $respDecoded = json_decode($response);

    if($respDecoded->success) {
        header('Location: ../confirmation.php?uuid='.$respDecoded->uuid);
    } else {
        header('Location: ../index.php');

    }


   
}

?>