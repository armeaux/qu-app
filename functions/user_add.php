<?php
session_start();
require_once '../includes/config.php';
require_once BASE_PATH . '/includes/auth_validate.php';

// Users class
require_once BASE_PATH . '/lib/Users/Users.php';
$users = new Users();

$edit = false;

// Serve POST request
if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	// Sanitize input post if we want
	$data_to_db = filter_input_array(INPUT_POST);

	// Check whether the user name already exists
	$db = getDbInstance();
	$db->where('user_name', $data_to_db['user_name']);
	$db->get('admin_accounts');

	if ($db->count >= 1)
	{
		$_SESSION['failure'] = 'Username already exists';
		header('location: ../user_add.php');
		exit;
	}

	// Encrypting the password
	$data_to_db['password'] = password_hash($data_to_db['password'], PASSWORD_DEFAULT);
	// Reset db instance
	$db = getDbInstance();
	$last_id = $db->insert('admin_accounts', $data_to_db);

	if ($last_id)
	{
		$_SESSION['success'] = 'Admin user added successfully';
		header('location: ../users.php');
		exit;
	}
}
?>
