<?php
session_start();
require_once './includes/config.php';
require_once 'includes/auth_validate.php';

$number_of_people	= filter_input(INPUT_GET, 'number_of_people');



//Get DB instance. function is defined in config.php
$db = getDbInstance();
$db->where('vendor_id', $_SESSION['vendor_id']);
$db->where('status', 'waiting');

if(isset($number_of_people)) {
    $db->where('number_of_people', $number_of_people);
}


$rows = $db->get('queue');

$totalNumberInQu = 0;
foreach($rows as $row) {
    $totalNumberInQu = $totalNumberInQu += $row['number_of_people'];
}

//Get Dashboard information

include_once('includes/vendor_home_header.php');


?>
   
    <div id="header">
    <div class="logo"><img src="assets/img/logo.png" width="150px" /></div>

    </div>

<div id="qu-wrapper">


    <div id="content">

        <div class="inline">
            <div></div>
      
        </div>

        <table cellspacing="0" cellpadding="0" class="table">
            <thead>
                <tr>
                    <th style="width: 50px;"><i class="fas fa-users"></th>
                    <th><i class="fas fa-user"></i></th>
                    <th><i class="fas fa-phone"></i></th>
                    <th><i class="fas fa-clock"></i></th>
                    <th style="text-align: right"><i class="fas fa-bell"></i></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($rows as $row): ?>
                    <?php 

                    $difInSeconds = strtotime("now") - strtotime($row['entered_datetime']);
                    $difInMinutes = round($difInSeconds / 60);
             


                    ?>


                <tr>
                    <td class="people" style="width: 50px; padding-left: 0px;"><div class="span"><b><?php echo $row['number_of_people']; ?></b></div></td>
                    <td class="name"><?php echo $row['customer_name']; ?></td>
                    <td><i><?php echo $row['phone_number']; ?></i></td>
                    <td><?php echo $difInMinutes; ?> min</td>
                    <td class="actions" style="text-align: right">
                        <a class="message" href="functions/acceptQu.php?uuid=<?php echo $row['uuid']; ?>">Message</a>
                        <a class="accept" href="functions/acceptQu.php?uuid=<?php echo $row['uuid']; ?>">Accept</a>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
 



<?php include_once('includes/footer.php'); ?>
