<fieldset>
    <div class="form-group">
        <label for="organisatie">Naam Organisatie</label>
          <input type="text" name="organisatie" value="<?php echo htmlspecialchars($edit ? $customer['organisatie'] : '', ENT_QUOTES, 'UTF-8'); ?>" placeholder="Stekkerdoos BV" class="form-control" required="required" id = "organisatie">
    </div> 

    <div class="form-group">
        <label for="website">Website</label>
          <input type="text" name="website" value="<?php echo htmlspecialchars($edit ? $customer['website'] : '', ENT_QUOTES, 'UTF-8'); ?>" placeholder="info@stekkerdoos.nl" class="form-control" required="required" id = "website">
    </div> 

    <div class="form-group">
        <label for="contactpersoon">Contactpersoon</label>
          <input type="text" name="contactpersoon" value="<?php echo htmlspecialchars($edit ? $customer['contactpersoon'] : '', ENT_QUOTES, 'UTF-8'); ?>" placeholder="Jan Stekker" class="form-control" required="required" id = "contactpersoon">
    </div> 

    <div class="form-group">
        <label for="email">Email</label>
          <input type="text" name="email" value="<?php echo htmlspecialchars($edit ? $customer['email'] : '', ENT_QUOTES, 'UTF-8'); ?>" placeholder="jan@stekkerdoos.nl" class="form-control" required="required" id = "email">
    </div> 

    <div class="form-group">
        <label for="telefoon">Telefoon</label>
          <input type="text" name="telefoon" value="<?php echo htmlspecialchars($edit ? $customer['telefoon'] : '', ENT_QUOTES, 'UTF-8'); ?>" placeholder="0703724829" class="form-control" required="required" id = "telefoon">
    </div> 

    <div class="form-group">
        <label for="adres">Adres</label>
          <input type="text" name="adres" value="<?php echo htmlspecialchars($edit ? $customer['adres'] : '', ENT_QUOTES, 'UTF-8'); ?>" placeholder="De Stekker 24" class="form-control" required="required" id = "adres">
    </div> 

    <div class="form-group">
        <label for="postcode">Postcode</label>
          <input type="text" name="postcode" value="<?php echo htmlspecialchars($edit ? $customer['postcode'] : '', ENT_QUOTES, 'UTF-8'); ?>" placeholder="5684SD" class="form-control" required="required" id = "postcode">
    </div> 

    <div class="form-group">
        <label for="stad">Stad</label>
          <input type="text" name="stad" value="<?php echo htmlspecialchars($edit ? $customer['stad'] : '', ENT_QUOTES, 'UTF-8'); ?>" placeholder="Stekkerdam" class="form-control" required="required" id = "stad">
    </div> 

    <div class="form-group">
        <label for="provincie">Provincie</label>
          <input type="text" name="provincie" value="<?php echo htmlspecialchars($edit ? $customer['provincie'] : '', ENT_QUOTES, 'UTF-8'); ?>" placeholder="Noord-Brabant" class="form-control" required="required" id = "provincie">
    </div> 

  

    <div class="form-group text-center">
        <label></label>
        <button type="submit" class="btn btn-warning" >Save <i class="glyphicon glyphicon-send"></i></button>
    </div>
</fieldset>
